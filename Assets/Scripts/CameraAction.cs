﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAction : MonoBehaviour
{
    CameraOrbit cam;
    bool zoom;
    float t;
    float min;
    float max;
    public float newDistance;
    public float speed;

	// Use this for initialization
	void Start ()
    {
        cam = GameObject.Find("Main Camera").GetComponent<CameraOrbit>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (zoom == true)
        //{
        //    t += speed * Time.deltaTime;
        //    cam.distance = Mathf.Lerp(min, max, t);
        //    if (t > 1.0f)
        //    {
        //        zoom = false;
        //        t = 0.0f;
        //    }
        //}
    }

    IEnumerator Zoom()
    {
        float dir = Mathf.Sign(cam.distance - newDistance);

        while (Mathf.Abs(cam.distance - newDistance) > 1f)
        {
            cam.distance += (speed * Time.deltaTime * -dir);
            yield return null;
        }

        cam.currentCameraAction = null;
        yield return null;
    }

    void OnTriggerEnter (Collider other)
    {
        //if (other.name == "Sphere")
        //{
        //    t = 0.0f;
        //    min = cam.distance;
        //    max = newDistance;
        //    if (zoom == false)
        //    {
        //        zoom = true;
        //    }
        //}

        if (cam.currentCameraAction != null)
            StopCoroutine(cam.currentCameraAction);            
        cam.currentCameraAction = StartCoroutine(Zoom());
    }

    //void OnTriggerExit(Collider other)
    //{
    //    if (other.name == "Sphere")
    //    {
    //        t = 0.0f;
    //        min = cam.distance;
    //        max = 20;
    //        if (zoom == false)
    //        {
    //            zoom = true;
    //        }
    //    }
    //}
}
